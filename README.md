Whether you are looking for a VA, FHA, USDA, Conventional, Jumbo, or Construction loan, Duluth mortgage professional Tamie Platts and her team of elite loan officers can help! Call her today and ask about down payment assistance programs and reverse mortgage options!
NMLS # 207270
Corp NMLS # 130562

Address: 3237 Satellite Blvd, Bldg 300, Suite 500, Duluth, GA 30096

Phone: 770-235-9907
